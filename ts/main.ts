/// <reference path="../typings/github-electron/github-electron.d.ts" />
'use strict';
import {
  app,
  BrowserWindow,
  crashReporter
} from 'electron';

let mainWindow: GitHubElectron.BrowserWindow = null;
crashReporter.start();

app.on('window-all-closed', () => {
  app.quit();
});

app.on('ready', () => {
  mainWindow = new BrowserWindow({'width': 600, 'height': 800});
  mainWindow.loadURL('file://' + __dirname + '/../renderer/index.html');
});
